from api_objects.base_api import BaseApi


class CalendarApi(BaseApi):

    # 新建日历
    def calendar_create(self, summary, description=None, permissions=None, color=None, summary_alias=None):
        url = f"{self.host}/open-apis/calendar/v4/calendars"
        data = {
            "summary": summary,
            "description": description,
            "permissions": permissions,
            "color": color,
            "summary_alias": summary_alias
        }
        return self.feishu_send("post", url, json=data)

    # 更新日历
    def calendar_update(self, calendar_id, summary=None, description=None, permissions=None, color=None,
                        summary_alias=None):
        url = f"{self.host}/open-apis/calendar/v4/calendars/{calendar_id}"
        data = {
            "summary": summary,
            "description": description,
            "permissions": permissions,
            "color": color,
            "summary_alias": summary_alias
        }
        return self.feishu_send("patch", url, json=data)

    # 删除日历
    def calendar_delete(self, calendar_id):
        url = f"{self.host}/open-apis/calendar/v4/calendars/{calendar_id}"
        return self.feishu_send("delete", url)

    # 查询日历
    def calendar_get(self, calendar_id):
        url = f"{self.host}/open-apis/calendar/v4/calendars/{calendar_id}"
        return self.feishu_send("get", url)

    # # 查询日历列表
    def calendar_get_list(self, page_size=50, page_token=None, sync_token=None):
        url = f"{self.host}/open-apis/calendar/v4/calendars"
        data = {
            "page_size": page_size,
            "page_token": page_token,
            "sync_token": sync_token
        }
        return self.feishu_send("get", url, json=data)

    # 搜索日历
    def calendar_search(self, query):
        url = f"{self.host}/open-apis/calendar/v4/calendars/search"
        data = {
            "query": query
        }
        return self.feishu_send("post", url, json=data)

    # 订阅日历
    def calendar_subscribe(self, calendar_id):
        url = f"{self.host}/open-apis/calendar/v4/calendars/{calendar_id}/subscribe"
        return self.feishu_send("post", url)

    # 取消订阅
    def calendar_unsubscribe(self, calendar_id):
        url = f"{self.host}/open-apis/calendar/v4/calendars/{calendar_id}/subscribe"
        return self.feishu_send("post", url)

    def calendar_subscription(self):
        url = f"{self.host}/open-apis/calendar/v4/calendars/subscription"
        return self.feishu_send("post", url)
