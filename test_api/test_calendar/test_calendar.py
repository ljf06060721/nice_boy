import allure
from hamcrest import *

from api_objects.Calendar.calendar_api import CalendarApi
from utils.data_name import DataName
from utils.json_value import JsonValue


@allure.feature("日历模块")
class TestCalendar:
    def setup_class(self):
        self.Calendar = CalendarApi()

    # 新建日历
    @allure.title("新增日历")
    def test_calendar_create(self):
        # 生成一个随机中文名称
        test_name = DataName.data_name()
        res = self.Calendar.calendar_create(test_name)
        # 新建日历生成一个calendar_id
        calendar_id = JsonValue.json_value(res, '$..calendar_id')
        # 查询日历列表获取所有的calendar_id
        re = self.Calendar.calendar_get(calendar_id)
        calendar_ids = JsonValue.json_value(re, '$..calendar_id')
        # 断言新增的日历可以通过查询接口查询到的日历列表中
        assert_that(calendar_id, is_in(calendar_ids))
        # 调用删除日历接口进行数据清理
        self.Calendar.calendar_delete(calendar_id)

    # 更新日历
    @allure.title("更新日历")
    def test_calendar_update(self):
        # 生成一个随机中文名称
        test_name = DataName.data_name()
        # 先调用新建日历接口创建一个日历
        calendar_id = JsonValue.json_value(self.Calendar.calendar_create(test_name), '$..calendar_id')
        # 更新日历
        summary = JsonValue.json_value(self.Calendar.calendar_update(calendar_id, summary="更新"), '$..summary')
        # 调用查询日历接口获取该日历信息
        new_summary = JsonValue.json_value(self.Calendar.calendar_get(calendar_id), '$..summary')
        # 断言更新的信息与查询的信息相同
        assert_that(summary, equal_to(new_summary))
        # 调用删除日历接口进行数据清理
        self.Calendar.calendar_delete(calendar_id)

    # 查询日历
    @allure.title("查询日历")
    def test_calendar_get(self):
        # 生成一个随机中文名称
        test_name = DataName.data_name()
        # 先调用新建日历接口创建一个日历
        calendar_id = JsonValue.json_value(self.Calendar.calendar_create(test_name), '$..calendar_id')
        # 调用查询日历接口获取该日历信息
        calendar_ids = JsonValue.json_value(self.Calendar.calendar_get(calendar_id), '$..calendar_id')
        # 断言新增的日历可以查询到
        assert_that(calendar_ids, contains_string(calendar_id))
        # 调用删除日历接口进行数据清理
        self.Calendar.calendar_delete(calendar_id)

    # 删除日历
    @allure.title("删除日历")
    def test_calendar_delete(self):
        # 生成一个随机中文名称
        test_name = DataName.data_name()
        # 先调用新建日历接口创建一个日历
        calendar_id = JsonValue.json_value(self.Calendar.calendar_create(test_name), '$..calendar_id')
        # 删除新建的日历
        self.Calendar.calendar_delete(calendar_id)
        # 查询该日历
        data = JsonValue.json_value(self.Calendar.calendar_get(calendar_id), '$.data')
        # 断言data中没有删除掉的calendar_id则表明删除成功
        assert_that(data, is_not(contains_string(calendar_id)))

    # 查询日历列表
    @allure.title("查询日历列表")
    def test_calendar_getlist(self):
        # 生成一个随机中文名称
        test_name = DataName.data_name()
        # 先调用新建日历接口创建一个日历
        calendar_id = JsonValue.json_value(self.Calendar.calendar_create(test_name), '$..calendar_id')
        # 调用查询日历接口获取该日历信息
        calendar_ids = JsonValue.json_value(self.Calendar.calendar_get_list(), '$..calendar_id')
        # 断言新增的日历在查询到的日历列表中
        assert_that(calendar_id, is_in(calendar_ids))
        # 调用删除日历接口进行数据清理
        self.Calendar.calendar_delete(calendar_id)

    # 搜索日历
    @allure.title("搜索日历")
    def test_calendar_search(self):
        # 生成一个随机中文名称
        test_name = DataName.data_name()
        # 先调用新建日历接口创建一个日历
        calendar_id = JsonValue.json_value(self.Calendar.calendar_create(test_name), '$..calendar_id')
        # 根据根据内容进行搜索日历获取搜索到的id
        new_calendar_id = JsonValue.json_value(self.Calendar.calendar_search("测试"), '$..calendar_id')
        # 搜索到的日历id长度最小是1，说明搜索成功
        assert_that(len(new_calendar_id), greater_than_or_equal_to(1))
        # 调用删除日历接口进行数据清理
        self.Calendar.calendar_delete(calendar_id)

    # 订阅日历
    @allure.title("订阅日历")
    def test_calendar_subscribe(self):
        # 生成一个随机中文名称
        test_name = DataName.data_name()
        # 先调用新建日历接口创建一个日历
        calendar_id = JsonValue.json_value(self.Calendar.calendar_create(test_name), '$..calendar_id')
        # 订阅这个日历
        res = self.Calendar.calendar_subscribe(calendar_id)
        # 断言code为0则表示订阅成功，暂时没有一个字段去表示日历的订阅状态，无法去具体判断该日历是否被订阅
        assert res['code'] == 0
        # 调用删除日历接口进行数据清理
        self.Calendar.calendar_delete(calendar_id)

    # 取消订阅日历
    @allure.title("取消订阅日历")
    def test_calendar_unsubscribe(self):
        # 生成一个随机中文名称
        test_name = DataName.data_name()
        # 先调用新建日历接口创建一个日历
        calendar_id = JsonValue.json_value(self.Calendar.calendar_create(test_name), '$..calendar_id')
        # 订阅这个日历
        self.Calendar.calendar_subscribe(calendar_id)
        # 取消订阅该日历
        res = self.Calendar.calendar_unsubscribe("feishu.cn_PVK4mwAnv9UfMLCcdxgPId@group.calendar.feishu.cn")
        # 断言code为0则表示取消订阅成功，暂时没有一个字段去表示日历的订阅状态，无法去具体判断该日历是否被订阅
        assert res['code'] == 0
        # 调用删除日历接口进行数据清理
        self.Calendar.calendar_delete(calendar_id)

    # 订阅日历变更事件
    @allure.title("订阅日历变更事件")
    def test_calendar_subscription(self):
        print(self.Calendar.calendar_subscription())
