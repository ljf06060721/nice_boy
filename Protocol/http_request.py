import requests as requests


class HttpRequest:
    # 封装请求方法
    @classmethod
    def send(cls, method, url, **data):
        return requests.request(method, url, **data).json()
