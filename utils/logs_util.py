import logging
from utils.file_util import FileUtil
import os

# 绑定logging的句柄
logger = logging.getLogger(__name__)
file_path = os.sep.join([FileUtil.get_project_path(), "logs"])
if not os.path.exists(file_path):
    os.mkdir(file_path)
# 拼接log文件夹的路径和句柄
fileHandler = logging.FileHandler(filename=file_path+'/apitest', encoding='utf-8')
# 日志的格式定义
formatter = logging.Formatter('[%(asctime)s] [%(levelname)s] %(message)s', '%Y-%m-%d %H:%M:%S')
# 文件绑定句柄格式
fileHandler.setFormatter(formatter)
# 设置控制台句柄
console = logging.StreamHandler()
# 设置控制台输入日志级别
console.setLevel(logging.INFO)
# 控制台句柄绑定日志日式
console.setFormatter(formatter)
# 添加内容到日志句柄中
logger.addHandler(fileHandler)
logger.addHandler(console)
logger.setLevel(logging.INFO)
