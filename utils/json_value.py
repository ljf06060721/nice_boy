
import jsonpath as jsonpath

from utils.logs_util import logger


class JsonValue:
    # 封装jsonpath提取json返回体的值
    @classmethod
    def json_value(cls, res, express):
        # res_content = json.loads(res)
        value = jsonpath.jsonpath(res, express)
        logger.info(f"请求响应内容中所取的值为{value}")
        if len(value) == 1:
            return value[0]
        else:
            return value
