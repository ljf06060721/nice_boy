import os.path

import yaml


class FileUtil:
    # 获取项目的绝对路径
    @classmethod
    def get_project_path(cls):
        return os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    # 将项目绝对路径和要读取的文件名称拼接得到文件的路径
    @classmethod
    def read_yaml(cls, filename):
        _path = os.sep.join([cls.get_project_path(), 'config', filename + '.yaml'])
        with open(_path, encoding='utf-8') as f:
            return yaml.safe_load(f)
