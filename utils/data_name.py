from faker import Faker

from utils.logs_util import logger


class DataName:
    # 生成一个随机名称
    @classmethod
    def data_name(cls):
        fake = Faker('zh_CN')
        name = fake.word()
        logger.info(f"随机生成的名称为:{name}")
        return name
